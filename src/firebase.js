import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyDRz-TlpL4BalBY3Twr1-I0DWXBE2Zf2RA",
    authDomain: "react-firebase-ts.firebaseapp.com",
    databaseURL: "https://react-firebase-ts.firebaseio.com",
    projectId: "react-firebase-ts",
    storageBucket: "react-firebase-ts.appspot.com",
    messagingSenderId: "156465127019",
    appId: "1:156465127019:web:21ef67fca73e2efffd1b2e"
}

firebase.initializeApp(config);
export default firebase;
