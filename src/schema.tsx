const schema = {
    schemas: {
        layoutSchema:{
            "name": "Layout Schema",
            "team": {
                "member1":{
                    "firstName": "Maneesh Parihar",
                    "phone": 123456789
                },
                "member2":{
                    "firstName": "Saurabh",
                    "phone": 123456789
                }
            }
        },
        implementationSchema:{
            "name": "Implementation Schema"
        }, 
        designSchema:{
            "name": "Design Schema"
        },
    }
}

export default schema