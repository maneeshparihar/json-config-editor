import React, { Fragment } from 'react';
import User from './User';
import firebase from './firebase';
import schema from './schema'
import Opts from './Opts';


interface Iprops {

}

interface Istate{
    schemaNameState: any[]
}

class Fbase extends React.Component<Iprops,Istate>{
    constructor(props:any){
        super(props);
        this.createSchemas = this.createSchemas.bind(this);
        this.state={
            schemaNameState : []
        }
    }

    public schemasLocal: Object = {};
    public schemasLocalArray: any[] = [];
    // public schemasLocalName: string = "";

    createSchemas():any{
        console.log("create schemas");
        firebase.database().ref().set(schema)
        .then(cb => {
            // console.log('cb after manual schema set', cb);
        })
    }

    populateOpts = function name(params:any) {
        
    }

    render(){
        // console.log("Fbase render called")
        return(
            <Fragment >
            <div className="App-header">
                <span>JSON Editor App</span> 
                <br />
                <button onClick={this.createSchemas}>Set Schemas from file to FB</button>
                <br />
                Existing Schemas
                <Opts options={this.state.schemaNameState} />
            </div>
            </Fragment>
        )
    }

    componentDidMount() {
        let root = firebase.database().ref();
        
        root.on('value', (snapshot)=>{
            this.schemasLocal = {};
            this.schemasLocalArray = [];
            this.schemasLocal = snapshot.val().schemas;
            console.log('schemasLocal', this.schemasLocal);
            Object.keys(this.schemasLocal).forEach(key=>{
                this.schemasLocalArray.push(
                    {   "key" : key.toString(), 
                        "name": snapshot.val().schemas[key].name.toString()
                    })
            })
            console.log('schemasLocalArray', this.schemasLocalArray);
            this.setState({
                schemaNameState : this.schemasLocalArray
            });
        })
    }
    componentWillUnmount() {  }

}

export default Fbase;