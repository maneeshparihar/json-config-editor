import React, {Fragment} from 'react';


class Opts extends React.Component<any>{
    constructor(props:any){
        super(props);

        this.handleChange = this.handleChange.bind(this);
    }
    selectedSchema: any;

    componentDidMount(){
    }

    componentDidUpdate(){
    }

    handleChange(e:any){
        console.log("e",e.target.value);
        this.selectedSchema = e.target.value;
    }

  

    render() {
        console.log('schema keys', this.props.options);
        let opx = this.props.options.map((opt:any)=>{
             return <option key={opt.key} value={opt.key}>{opt.name}</option>
        })
        let isLoggedIn = false;
        return(
    <div>
            <select onChange={this.handleChange}>
                {opx}
            </select>
                <br /><br />
            <button onClick={this.handleChange}>Fetch JSON for Selected Schema (pending)</button>
    </div>
        )
    }
}

export default Opts;